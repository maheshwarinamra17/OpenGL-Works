#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glut.h>
#include "glm.h"
#define PI 3.1416

float x=0, y=0, z=0, graus=0;
int fps=0, displayList=0;

GLfloat lightPos[4] = {50.0, 30.0 ,0.0, 0.0};
GLfloat lightAmb[3] = {0.1, 0.1, 0.1};
GLfloat lightDiff[3] = {1.0, 1.0, 1.0};

GLMmodel* objloader;

void keyboard(unsigned char keys, int xx, int yy) {
	glutPostRedisplay();
	switch(keys) {
		case 'a' : x-=2; break; // Move object towards negative X-axis
		case 'd' : x+=2; break; // Move object towards positive X-axis
		case 's' : z+=2; break; // Move object towards positive Z-axis
		case 'w' : z-=2; break; // Move object towards negative Z-axis
		case '.' : y-=2; break; // Zoom Out
		case ',' : y+=2; break; // Zoom In
		case 'k' : graus-=2.0; break; // Rotate clockwise
		case 'i' : graus+=2.0; break; // Rotate anticlockwise
		case 27  : exit(0); // Exit
	}
}

void changeSize(int w, int h) {
	if(h == 0) h = 1;
	float ratio = 1.0* w / h;
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    	glViewport(0, 0, w, h);
	gluPerspective(25, ratio, 1, 1000);
	glMatrixMode(GL_MODELVIEW);
}


void renderScene(void) {
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//Setting Look at vector
	glLoadIdentity();
	gluLookAt(15.0, 30.0,	15.0,		
			  0.0,	5.0,	0.0,		
		      0.0f,	1.0f,	0.0f);		
	
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmb);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiff);
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); 
	glTranslatef(x,y,z);
	glRotatef(graus, 1.0, 0.0, 0.0);
	
	//Using Display lists method for loading vertex 
	glCallList(displayList);
	
	glutSwapBuffers();
}

int main(int argc, char **argv) {
	// OpenGL initialization
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(800,600);
	glutCreateWindow("Objloader Prototype");
	
	// Defining hardware functions
	glutDisplayFunc(renderScene);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(renderScene);
	glutReshapeFunc(changeSize);
	
	// Enable Lighting
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);
	
	
	objloader = (GLMmodel*)malloc(sizeof(GLMmodel));
	objloader = glmReadOBJ("./obj/phobos.obj");
	
	displayList=glGenLists(1);
	glNewList(displayList,GL_COMPILE);
	glmDraw(objloader, GLM_SMOOTH | GLM_MATERIAL);
	glEndList();
	
	glutMainLoop();
	
	return 0;
}

